/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package circulo;

/**
 *
 * @author Paco Aldarias
 * <a href="paco.aldarias@ceedcv.es">paco.aldarias@ceedcv.es </a>
 *
 * @version 1.0
 */
public class Circulo {

	private double centroX;

	private double centroY;

	private double radio;

	/**
	 * Constructor
	 *
	 * @param cx centro coordenada X
	 * @param cy centro coordenada Y
	 * @param r radio
	 */
	public Circulo(double cx, double cy, double r) {

		centroX = cx;

		centroY = cy;

		radio = r;

	}

	/**
	 * Getter
	 *
	 * @return centro coordenada X
	 */
	public double getCentroX() {

		return centroX;

	}

	/**
	 * Calcula la longuitud de la circunferencia
	 *
	 * @return circunferencia
	 */
	public double getCircunferencia() {

		return 2 * Math.PI * radio;

	}

	/**
	 * Desplaza el circulo a otro lado
	 *
	 * @param deltaX movimiento en el eje X
	 * @param deltaY movimiento en el eje Y
	 */
	public void mueve(double deltaX, double deltaY) {

		centroX = centroX + deltaX;

		centroY = centroY + deltaY;

	}

	/*
    * Escala en el circulo
    * @param s factor de escala
	 */
	public void escala(double s) {

		radio = radio * s;

	}

}
